﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Main
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Main))
        Me.gamePanel = New System.Windows.Forms.Panel()
        Me.snakeMenu = New System.Windows.Forms.MainMenu(Me.components)
        Me.MNgame = New System.Windows.Forms.MenuItem()
        Me.MInewgame = New System.Windows.Forms.MenuItem()
        Me.MIpause = New System.Windows.Forms.MenuItem()
        Me.MIsetting = New System.Windows.Forms.MenuItem()
        Me.MNdebug = New System.Windows.Forms.MenuItem()
        Me.MIpoints = New System.Windows.Forms.MenuItem()
        Me.MNabout = New System.Windows.Forms.MenuItem()
        Me.MIabout = New System.Windows.Forms.MenuItem()
        Me.scorePanel = New System.Windows.Forms.Panel()
        Me.scoreLabel = New System.Windows.Forms.Label()
        Me.reverseCD = New System.Windows.Forms.Label()
        Me.scorePanel.SuspendLayout()
        Me.SuspendLayout()
        '
        'gamePanel
        '
        Me.gamePanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gamePanel.Location = New System.Drawing.Point(0, 0)
        Me.gamePanel.Margin = New System.Windows.Forms.Padding(0)
        Me.gamePanel.Name = "gamePanel"
        Me.gamePanel.Size = New System.Drawing.Size(640, 439)
        Me.gamePanel.TabIndex = 0
        '
        'snakeMenu
        '
        Me.snakeMenu.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MNgame, Me.MNdebug, Me.MNabout})
        '
        'MNgame
        '
        Me.MNgame.Index = 0
        Me.MNgame.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MInewgame, Me.MIpause, Me.MIsetting})
        Me.MNgame.Text = "Game"
        '
        'MInewgame
        '
        Me.MInewgame.Index = 0
        Me.MInewgame.Shortcut = System.Windows.Forms.Shortcut.F4
        Me.MInewgame.Text = "New Game"
        '
        'MIpause
        '
        Me.MIpause.Enabled = False
        Me.MIpause.Index = 1
        Me.MIpause.Shortcut = System.Windows.Forms.Shortcut.F5
        Me.MIpause.Text = "Pause"
        '
        'MIsetting
        '
        Me.MIsetting.Index = 2
        Me.MIsetting.Text = "Setting"
        '
        'MNdebug
        '
        Me.MNdebug.Index = 1
        Me.MNdebug.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MIpoints})
        Me.MNdebug.Text = "Debug"
        '
        'MIpoints
        '
        Me.MIpoints.Index = 0
        Me.MIpoints.Text = "Points"
        '
        'MNabout
        '
        Me.MNabout.Index = 2
        Me.MNabout.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MIabout})
        Me.MNabout.Text = "About"
        '
        'MIabout
        '
        Me.MIabout.Index = 0
        Me.MIabout.Text = "About"
        '
        'scorePanel
        '
        Me.scorePanel.BackColor = System.Drawing.Color.Gainsboro
        Me.scorePanel.Controls.Add(Me.reverseCD)
        Me.scorePanel.Controls.Add(Me.scoreLabel)
        Me.scorePanel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.scorePanel.Location = New System.Drawing.Point(0, 439)
        Me.scorePanel.Margin = New System.Windows.Forms.Padding(0)
        Me.scorePanel.Name = "scorePanel"
        Me.scorePanel.Size = New System.Drawing.Size(640, 20)
        Me.scorePanel.TabIndex = 0
        '
        'scoreLabel
        '
        Me.scoreLabel.AutoSize = True
        Me.scoreLabel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.scoreLabel.Location = New System.Drawing.Point(0, 0)
        Me.scoreLabel.Name = "scoreLabel"
        Me.scoreLabel.Padding = New System.Windows.Forms.Padding(2)
        Me.scoreLabel.Size = New System.Drawing.Size(46, 19)
        Me.scoreLabel.TabIndex = 0
        Me.scoreLabel.Text = "Score:"
        '
        'reverseCD
        '
        Me.reverseCD.AutoSize = True
        Me.reverseCD.Dock = System.Windows.Forms.DockStyle.Right
        Me.reverseCD.Location = New System.Drawing.Point(553, 0)
        Me.reverseCD.Name = "reverseCD"
        Me.reverseCD.Size = New System.Drawing.Size(87, 15)
        Me.reverseCD.TabIndex = 1
        Me.reverseCD.Text = "Reverse CD: 0"
        '
        'Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(640, 459)
        Me.Controls.Add(Me.gamePanel)
        Me.Controls.Add(Me.scorePanel)
        Me.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Menu = Me.snakeMenu
        Me.Name = "Main"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Snake"
        Me.scorePanel.ResumeLayout(False)
        Me.scorePanel.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gamePanel As System.Windows.Forms.Panel
    Friend WithEvents snakeMenu As System.Windows.Forms.MainMenu
    Friend WithEvents MNgame As System.Windows.Forms.MenuItem
    Friend WithEvents MNdebug As System.Windows.Forms.MenuItem
    Friend WithEvents MIpoints As System.Windows.Forms.MenuItem
    Friend WithEvents MInewgame As System.Windows.Forms.MenuItem
    Friend WithEvents MIpause As System.Windows.Forms.MenuItem
    Friend WithEvents MNabout As System.Windows.Forms.MenuItem
    Friend WithEvents MIabout As System.Windows.Forms.MenuItem
    Friend WithEvents MIsetting As System.Windows.Forms.MenuItem
    Friend WithEvents scorePanel As System.Windows.Forms.Panel
    Friend WithEvents scoreLabel As System.Windows.Forms.Label
    Friend WithEvents reverseCD As System.Windows.Forms.Label

End Class
