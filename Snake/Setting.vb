﻿Public Class Setting
    Private key_Up As Keys = Keys.Up
    Private key_Down As Keys = Keys.Down
    Private key_Left As Keys = Keys.Left
    Private key_Right As Keys = Keys.Right
    Private has_Wall As Boolean = True
    Private window_scale As Integer = 10
    Private speed_up As Boolean = False
    Public Property keyUp As Keys
        Get
            Return key_Up
        End Get
        Set(value As Keys)
            key_Up = value
        End Set
    End Property
    Public Property keyDown As Keys
        Get
            Return key_Down
        End Get
        Set(value As Keys)
            key_Down = value
        End Set
    End Property
    Public Property keyLeft As Keys
        Get
            Return key_Left
        End Get
        Set(value As Keys)
            key_Left = value
        End Set
    End Property
    Public Property keyRight As Keys
        Get
            Return key_Right
        End Get
        Set(value As Keys)
            key_Right = value
        End Set
    End Property
    Public Property hasWall As Boolean
        Get
            Return has_Wall
        End Get
        Set(value As Boolean)
            has_Wall = value
        End Set
    End Property
    Public Property scale As Integer
        Get
            Return window_scale
        End Get
        Set(value As Integer)
            window_scale = value
        End Set
    End Property
    Public Property speedUp As Boolean
        Get
            Return speed_up
        End Get
        Set(value As Boolean)
            speed_up = value
        End Set
    End Property
End Class
