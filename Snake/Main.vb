﻿Public Class Main

    Public Shared snakeConfig As Setting = New Setting
    Private countTimer As Timer = New Timer
    Private currentWay As Way = Way.LEFT
    Private tempWay As Way = Way.LEFT
    Private snake As ArrayList
    Private emptySpace As ArrayList
    Private hills As ArrayList
    Private fruit As Point = New Point(640, 480)
    Private bonus As Point = New Point(640, 480)
    Private currentBonus As BonusFruit = BonusFruit.NONE
    Private score As Integer
    Private gameStarted As Boolean = False
    Private rand As New Random()
    Private baseWidth As Integer = 64
    Private baseHeight As Integer = 48
    Private reverse As Boolean = False
    Private speedUp As Integer = 0
    Private speed As Integer = 250
    Private t As Threading.Thread = New Threading.Thread(AddressOf holdKey)
    Private reverseTime As Integer = 0
    Private reverseTimer As Timer = New Timer
    Private Enum Way
        LEFT
        RIGHT
        UP
        DOWN
    End Enum
    Private Enum Collision
        NONE
        WALL
        SNAKE
        HILL
    End Enum
    Private Enum BonusFruit
        NONE
        BLUE
        GREEN
        YELLOW
        REVERSE
    End Enum

    Private Sub Main_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        If gameStarted Then
            gameResume()
        End If
    End Sub

    Private Sub Main_Deactivate(sender As Object, e As EventArgs) Handles Me.Deactivate
        If gameStarted Then
            gamePause()
        End If
    End Sub

    Private Sub Main_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        saveConfig()
    End Sub

    Private Sub Main_KeyUp(sender As Object, e As KeyEventArgs) Handles Me.KeyUp
        If snakeConfig.speedUp Then
            t.Abort()
            speedUp = 0
        End If
    End Sub

    Private Sub holdKey()
        While t.IsAlive
            speedUp = 200
        End While
    End Sub

    Private Sub Main_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If (Not gameStarted) Then
            Return
        End If
        If snakeConfig.speedUp AndAlso (e.KeyCode = snakeConfig.keyDown Or e.KeyCode = snakeConfig.keyLeft Or e.KeyCode = snakeConfig.keyRight Or e.KeyCode = snakeConfig.keyUp) Then
            t = New Threading.Thread(AddressOf holdKey)
            t.Start()
        End If
        Dim p0 As Point = snake(0)
        Dim p1 As Point = snake(1)
        Select Case e.KeyCode
            Case snakeConfig.keyUp
                If Not reverse Then
                    If (Not currentWay = Way.DOWN) Then
                        tempWay = Way.UP
                    End If
                Else
                    If (Not currentWay = Way.UP) Then
                        tempWay = Way.DOWN
                    End If
                End If
            Case snakeConfig.keyLeft
                If Not reverse Then
                    If (Not currentWay = Way.RIGHT) Then
                        tempWay = Way.LEFT
                    End If
                Else
                    If (Not currentWay = Way.LEFT) Then
                        tempWay = Way.RIGHT
                    End If
                End If
            Case snakeConfig.keyDown
                If Not reverse Then
                    If (Not currentWay = Way.UP) Then
                        tempWay = Way.DOWN
                    End If
                Else
                    If (Not currentWay = Way.DOWN) Then
                        tempWay = Way.UP
                    End If
                End If
            Case snakeConfig.keyRight
                If Not reverse Then
                    If (Not currentWay = Way.LEFT) Then
                        tempWay = Way.RIGHT
                    End If
                Else
                    If (Not currentWay = Way.RIGHT) Then
                        tempWay = Way.LEFT
                    End If
                End If
        End Select
    End Sub

    Private Sub drawOnForm()
        Dim fruitBrush As New SolidBrush(Color.Red)
        Dim blueBrush As New SolidBrush(Color.Blue)
        Dim greenBrush As New SolidBrush(Color.Green)
        Dim yellowBrush As New SolidBrush(Color.Orange)
        Dim blackBrush As New SolidBrush(Color.Black)
        Dim whiteBrush As New SolidBrush(Color.White)
        Dim g As Graphics
        Dim bm As Bitmap = New Bitmap(baseWidth * snakeConfig.scale, baseHeight * snakeConfig.scale)
        g = Graphics.FromImage(bm)
        'g = Me.gamePanel.CreateGraphics()
        g.Clear(Color.White)
        For Each p As Point In snake
            g.FillRectangle(blackBrush, p.X, p.Y, snakeConfig.scale, snakeConfig.scale)
            'g.FillRectangle(whiteBrush, p.X + 4, p.Y, 1, 10)
            'g.FillRectangle(whiteBrush, p.X + 9, p.Y, 1, 10)
            'g.FillRectangle(whiteBrush, p.X, p.Y + 4, 10, 1)
            'g.FillRectangle(whiteBrush, p.X, p.Y + 9, 10, 1)
        Next
        For Each p As Point In hills
            For i As Integer = p.Y To p.Y + snakeConfig.scale Step 2
                g.DrawLine(Pens.Gray, p.X, i, p.X + snakeConfig.scale, i)
            Next
        Next
        Select Case currentBonus
            Case BonusFruit.BLUE
                g.FillRectangle(blueBrush, bonus.X, bonus.Y, snakeConfig.scale, snakeConfig.scale)
            Case BonusFruit.GREEN
                g.FillRectangle(greenBrush, bonus.X, bonus.Y, snakeConfig.scale, snakeConfig.scale)
            Case BonusFruit.YELLOW
                g.FillRectangle(yellowBrush, bonus.X, bonus.Y, snakeConfig.scale, snakeConfig.scale)
            Case BonusFruit.REVERSE
                Dim sf As New StringFormat
                sf.LineAlignment = StringAlignment.Center
                sf.Alignment = StringAlignment.Center
                Dim f As Font = New Font("Arial", snakeConfig.scale * 1.5, FontStyle.Bold, GraphicsUnit.Pixel)
                g.DrawString("?", f, blackBrush, bonus.X + snakeConfig.scale / 2, bonus.Y + snakeConfig.scale / 2, sf)
                sf.Dispose()
                f.Dispose()
        End Select
        g.FillRectangle(fruitBrush, fruit.X, fruit.Y, snakeConfig.scale, snakeConfig.scale)
        blackBrush.Dispose()
        fruitBrush.Dispose()
        blueBrush.Dispose()
        greenBrush.Dispose()
        yellowBrush.Dispose()
        g = Me.gamePanel.CreateGraphics()
        g.DrawImage(bm, 0, 0)
        bm.Dispose()
        g.Dispose()
        GC.Collect()
    End Sub

    Public Sub newGame()
        currentWay = Way.LEFT
        tempWay = Way.LEFT
        gameResume()
        countTimer.Stop()
        gameStarted = True
        score = 0
        t.Abort()
        speedUp = 0
        emptySpace = New ArrayList
        hills = New ArrayList
        For i As Integer = 0 To (baseWidth - 1) * snakeConfig.scale Step snakeConfig.scale
            For j As Integer = 0 To (baseHeight - 1) * snakeConfig.scale Step snakeConfig.scale
                emptySpace.Add(New Point(i, j))
            Next
        Next
        snake = New ArrayList
        For i As Integer = 27 * snakeConfig.scale To 36 * snakeConfig.scale Step snakeConfig.scale
            snake.Add(New Point(i, (baseHeight / 2) * snakeConfig.scale))
            emptySpace.Remove(New Point(i, (baseHeight / 2) * snakeConfig.scale))
        Next
        reverse = False
        addFruit()
        changeScore(score)
        countTimer = New Timer
        AddHandler countTimer.Tick, AddressOf animate
        speed = 250
        countTimer.Interval = speed
        countTimer.Enabled = True
        countTimer.Start()
        reverseTimer.Stop()
        reverseTime = 0
        reverseCD.Text = "Reverse CD: " + reverseTime.ToString
        reverseTimer = New Timer
    End Sub

    Private Sub animate()
        countTimer.Interval = IIf(speed > speedUp, speed - speedUp, 1)
        currentWay = tempWay
        Dim p As Point = snake(0)
        Dim ins As Point
        Select Case currentWay
            Case Way.DOWN
                ins = New Point(p.X, p.Y + snakeConfig.scale)
            Case Way.LEFT
                ins = New Point(p.X - snakeConfig.scale, p.Y)
            Case Way.RIGHT
                ins = New Point(p.X + snakeConfig.scale, p.Y)
            Case Way.UP
                ins = New Point(p.X, p.Y - snakeConfig.scale)
        End Select
        If ((Not checkCollision(ins) = Collision.NONE) AndAlso snakeConfig.hasWall) Or ((Not snakeConfig.hasWall) AndAlso (checkCollision(ins) = Collision.SNAKE Or checkCollision(ins) = Collision.HILL)) Then
            gameStarted = False
            MIpause.Enabled = False
            countTimer.Stop()
            Dim g As Graphics
            g = Me.gamePanel.CreateGraphics()
            g.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias
            Dim sf As New StringFormat
            sf.LineAlignment = StringAlignment.Center
            sf.Alignment = StringAlignment.Center
            Dim f As Font = New Font("Consolas", 8 * snakeConfig.scale, FontStyle.Bold, GraphicsUnit.Pixel)
            g.DrawString("Game Over", f, Brushes.Black, New RectangleF(0, 20 * snakeConfig.scale, baseWidth * snakeConfig.scale, 8 * snakeConfig.scale), sf)
            Dim fs As Font = New Font("Consolas", 3 * snakeConfig.scale, FontStyle.Regular, GraphicsUnit.Pixel)
            g.DrawString("Score: " + score.ToString, fs, Brushes.Orange, New RectangleF(0, 28 * snakeConfig.scale, baseWidth * snakeConfig.scale, 3 * snakeConfig.scale), sf)
            sf.Dispose()
            f.Dispose()
            fs.Dispose()
            g.Dispose()
        Else
            If checkCollision(ins) = Collision.WALL Then
                Select Case currentWay
                    Case Way.DOWN
                        ins = New Point(ins.X, 0)
                    Case Way.LEFT
                        ins = New Point((baseWidth - 1) * snakeConfig.scale, ins.Y)
                    Case Way.RIGHT
                        ins = New Point(0, ins.Y)
                    Case Way.UP
                        ins = New Point(ins.X, (baseHeight - 1) * snakeConfig.scale)
                End Select
            End If
            emptySpace.Remove(ins)
            snake.Insert(0, ins)
            If (checkFruit(ins)) Then
                addFruit()
            Else
                snake.RemoveAt(snake.Count - 1)
                emptySpace.Add(snake(snake.Count - 1))
            End If
            drawOnForm()
        End If
    End Sub

    Private Function checkFruit(p As Point) As Boolean
        If p.X = bonus.X AndAlso p.Y = bonus.Y Then
            score = score + 10
            Select Case currentBonus
                Case BonusFruit.YELLOW
                    speed = speed + 20
                Case BonusFruit.GREEN
                    speed = IIf(speed <= 40, 20, speed - 20)
                Case BonusFruit.BLUE
                    snake.RemoveAt(snake.Count - 1)
                Case BonusFruit.REVERSE
                    score = score + 30
                    reverse = True
                    reverseTimer.Stop()
                    reverseTimer = New Timer
                    reverseTimer.Interval = 1000
                    reverseTime = 60
                    AddHandler reverseTimer.Tick, AddressOf reverseTick
                    reverseTimer.Start()
            End Select
            emptySpace.Add(snake(snake.Count - 1))
            addBonus()
            changeScore(score)
        End If
        If p.X = fruit.X AndAlso p.Y = fruit.Y Then
            score = score + 20
            speed = IIf(speed <= 10, 10, speed - 10)
            changeScore(score)
            If Not snakeConfig.hasWall AndAlso rand.Next(1, 6) = 1 Then
                Dim index As Integer = rand.Next(emptySpace.Count)
                Dim tp As Point = CType(emptySpace(index), Point)
                If p.X > tp.X + 2 Or p.X < tp.X - 2 _
                    Or p.Y > tp.Y + 2 Or p.Y < tp.Y - 2 Then
                    emptySpace.RemoveAt(index)
                    hills.Add(tp)
                End If
            End If
            Return True
        End If
        Return False
    End Function

    Private Function checkCollision(p As Point) As Collision
        If snake.Contains(p) Then
            Return Collision.SNAKE
        End If
        If hills.Contains(p) Then
            Return Collision.HILL
        End If
        If p.X = gamePanel.Location.X - snakeConfig.scale Or p.X = baseWidth * snakeConfig.scale _
            Or p.Y = gamePanel.Location.Y - snakeConfig.scale Or p.Y = baseHeight * snakeConfig.scale Then
            Return Collision.WALL
        End If
        Return Collision.NONE
    End Function

    Private Sub addFruit()
        If (emptySpace.Count <> 0) Then
            Dim index As Integer = rand.Next(emptySpace.Count)
            fruit = emptySpace(index)
            addBonus()
        End If
    End Sub

    Private Sub addBonus()
        If (emptySpace.Count <> 0) Then
            Dim i As Integer = rand.Next(1, 11)
            If (i = 1) Then
                Dim index As Integer = rand.Next(emptySpace.Count)
                bonus = emptySpace(index)
                Dim j As Integer = rand.Next(1, 5)
                currentBonus = j
            Else
                bonus = New Point(baseWidth * snakeConfig.scale, baseHeight * snakeConfig.scale)
                currentBonus = BonusFruit.NONE
            End If
        End If
    End Sub

    Private Sub saveConfig()
        Dim sw As New IO.StreamWriter(Application.StartupPath + "\config.xml")
        Dim xs As New Xml.Serialization.XmlSerializer(snakeConfig.GetType)
        xs.Serialize(sw, snakeConfig)
        sw.Close()
    End Sub

    Private Sub Main_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        If Not IO.File.Exists(Application.StartupPath + "\config.xml") Then
            saveConfig()
        End If
        Dim s As Xml.Serialization.XmlSerializer = New Xml.Serialization.XmlSerializer(GetType(Setting))
        Dim sr As New IO.StreamReader(Application.StartupPath + "\config.xml", System.Text.Encoding.Default)
        snakeConfig = DirectCast(s.Deserialize(sr), Setting)
        sr.Close()

        rescale()
        Dim g As Graphics
        g = Me.gamePanel.CreateGraphics
        Dim sf As New StringFormat
        sf.LineAlignment = StringAlignment.Center
        sf.Alignment = StringAlignment.Center
        Dim f As Font = New Font("Consolas", 8 * snakeConfig.scale, FontStyle.Bold, GraphicsUnit.Pixel)
        g.DrawString("Snake", f, Brushes.Black, New RectangleF(0, 20 * snakeConfig.scale, baseWidth * snakeConfig.scale, 8 * snakeConfig.scale), sf)
        sf.Dispose()
        f.Dispose()
        g.Dispose()
        AddHandler MNgame.Popup, AddressOf gamePause
        AddHandler MNdebug.Popup, AddressOf gamePause
        AddHandler MNabout.Popup, AddressOf gamePause
        AddHandler snakeMenu.Collapse, AddressOf gameResume
    End Sub

    Public Sub gamePause()
        If (Not gameStarted) Then
            Return
        End If
        Me.Text = "Snake [Paused]"
        countTimer.Enabled = False
    End Sub

    Public Sub gameResume()
        If (Not gameStarted) Then
            Return
        End If
        Me.Text = "Snake"
        countTimer.Enabled = True
    End Sub

    Private Sub MIpoints_Click(sender As Object, e As EventArgs) Handles MIpoints.Click
        gamePause()
        MessageBox.Show("Fruit:" + vbNewLine + "    X: " + fruit.X.ToString + vbNewLine + "    Y: " + fruit.Y.ToString _
                        + vbNewLine _
                        + "Bonus:" + vbNewLine + "    X: " + bonus.X.ToString + vbNewLine + "    Y: " + bonus.Y.ToString)
        gameResume()
        MIpause.Text = "Pause"
    End Sub

    Private Sub MIpause_Click(sender As Object, e As EventArgs) Handles MIpause.Click
        If MIpause.Text = "Pause" Then
            gamePause()
            MIpause.Text = "Resume"
        Else
            gameResume()
            MIpause.Text = "Pause"
        End If
    End Sub

    Private Sub MInewgame_Click(sender As Object, e As EventArgs) Handles MInewgame.Click
        newGame()
        MIpause.Enabled = True
    End Sub

    Private Sub MIabout_Click(sender As Object, e As EventArgs) Handles MIabout.Click
        MessageBox.Show("Author: Carl Kuang" + vbNewLine + "Version: 1.5", "About", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
    End Sub

    Private Sub MIsetting_Click(sender As Object, e As EventArgs) Handles MIsetting.Click
        Config.Owner = Me
        Config.ShowDialog()
    End Sub

    Public Sub rescale()
        Me.ClientSize = New Size(baseWidth * snakeConfig.scale, baseHeight * snakeConfig.scale + 20)
    End Sub

    Private Sub changeScore(ByVal score As Integer)
        scoreLabel.Text = "Score: " + score.ToString
    End Sub

    Private Sub reverseTick()
        If reverseTime > 0 Then
            reverseTime = reverseTime - 1
        End If
        If reverseTime = 0 Then
            reverseTimer.Stop()
            reverse = False
        End If
        reverseCD.Text = "Reverse CD: " + reverseTime.ToString
    End Sub

    Private Sub Main_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If snakeConfig.speedUp Then
            t.Abort()
            speedUp = 0
        End If
    End Sub
End Class
