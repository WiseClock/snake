﻿Public Class Config

    Private doRestart As Boolean = False

    Private Sub Config_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        Main.snakeConfig.scale = IIf(scale3x.Checked, 3, IIf(scale5x.Checked, 5, 10))
        If doRestart Then
            Main.rescale()
            Main.newGame()
        End If
    End Sub

    Private Sub Config_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        change()
        doRestart = False
    End Sub

    Private Sub hasWall_CheckedChanged(sender As Object, e As EventArgs) Handles hasWall.CheckedChanged
        Main.snakeConfig.hasWall = hasWall.Checked
    End Sub

    Private Sub upBox_KeyUp(sender As Object, e As KeyEventArgs) Handles upBox.KeyUp
        Main.snakeConfig.keyUp = e.KeyCode
        change()
    End Sub

    Private Sub downBox_KeyUp(sender As Object, e As KeyEventArgs) Handles downBox.KeyUp
        Main.snakeConfig.keyDown = e.KeyCode
        change()
    End Sub

    Private Sub leftBox_KeyUp(sender As Object, e As KeyEventArgs) Handles leftBox.KeyUp
        Main.snakeConfig.keyLeft = e.KeyCode
        change()
    End Sub

    Private Sub rightBox_KeyUp(sender As Object, e As KeyEventArgs) Handles rightBox.KeyUp
        Main.snakeConfig.keyRight = e.KeyCode
        change()
    End Sub

    Private Sub change()
        upBox.Text = Main.snakeConfig.keyUp.ToString
        downBox.Text = Main.snakeConfig.keyDown.ToString
        leftBox.Text = Main.snakeConfig.keyLeft.ToString
        rightBox.Text = Main.snakeConfig.keyRight.ToString
        hasWall.Checked = Main.snakeConfig.hasWall
        speedUp.Checked = Main.snakeConfig.speedUp
        scale3x.Checked = (Main.snakeConfig.scale = 3)
        scale5x.Checked = (Main.snakeConfig.scale = 5)
        scale10x.Checked = (Main.snakeConfig.scale = 10)
    End Sub

    Private Sub radioClick(sender As Object, e As EventArgs) Handles scale3x.Click, scale5x.Click, scale10x.Click
        If (Not doRestart AndAlso Not restart()) Then
            change()
        Else
            doRestart = True
        End If
    End Sub

    Private Function restart() As Boolean
        If MessageBox.Show("Do you wish to restart the game?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            Return True
        End If
        Return False
    End Function

    Private Sub speedUp_CheckedChanged(sender As Object, e As EventArgs) Handles speedUp.CheckedChanged
        Main.snakeConfig.speedUp = speedUp.Checked
    End Sub
End Class