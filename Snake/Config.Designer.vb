﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Config
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.upBox = New System.Windows.Forms.TextBox()
        Me.downBox = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.leftBox = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.rightBox = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.hasWall = New System.Windows.Forms.CheckBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.scale10x = New System.Windows.Forms.RadioButton()
        Me.scale5x = New System.Windows.Forms.RadioButton()
        Me.scale3x = New System.Windows.Forms.RadioButton()
        Me.speedUp = New System.Windows.Forms.CheckBox()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(27, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "UP:"
        '
        'upBox
        '
        Me.upBox.Location = New System.Drawing.Point(63, 6)
        Me.upBox.Name = "upBox"
        Me.upBox.ReadOnly = True
        Me.upBox.Size = New System.Drawing.Size(100, 21)
        Me.upBox.TabIndex = 1
        '
        'downBox
        '
        Me.downBox.Location = New System.Drawing.Point(63, 33)
        Me.downBox.Name = "downBox"
        Me.downBox.ReadOnly = True
        Me.downBox.Size = New System.Drawing.Size(100, 21)
        Me.downBox.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 36)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(48, 15)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "DOWN:"
        '
        'leftBox
        '
        Me.leftBox.Location = New System.Drawing.Point(63, 60)
        Me.leftBox.Name = "leftBox"
        Me.leftBox.ReadOnly = True
        Me.leftBox.Size = New System.Drawing.Size(100, 21)
        Me.leftBox.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 63)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(38, 15)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "LEFT:"
        '
        'rightBox
        '
        Me.rightBox.Location = New System.Drawing.Point(63, 87)
        Me.rightBox.Name = "rightBox"
        Me.rightBox.ReadOnly = True
        Me.rightBox.Size = New System.Drawing.Size(100, 21)
        Me.rightBox.TabIndex = 7
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(12, 90)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(46, 15)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "RIGHT:"
        '
        'hasWall
        '
        Me.hasWall.AutoSize = True
        Me.hasWall.Location = New System.Drawing.Point(12, 169)
        Me.hasWall.Name = "hasWall"
        Me.hasWall.Size = New System.Drawing.Size(63, 19)
        Me.hasWall.TabIndex = 8
        Me.hasWall.Text = "Border"
        Me.hasWall.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.scale10x)
        Me.GroupBox1.Controls.Add(Me.scale5x)
        Me.GroupBox1.Controls.Add(Me.scale3x)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 114)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(151, 49)
        Me.GroupBox1.TabIndex = 9
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "scale"
        '
        'scale10x
        '
        Me.scale10x.AutoSize = True
        Me.scale10x.Location = New System.Drawing.Point(101, 20)
        Me.scale10x.Name = "scale10x"
        Me.scale10x.Size = New System.Drawing.Size(44, 19)
        Me.scale10x.TabIndex = 2
        Me.scale10x.TabStop = True
        Me.scale10x.Text = "10x"
        Me.scale10x.UseVisualStyleBackColor = True
        '
        'scale5x
        '
        Me.scale5x.AutoSize = True
        Me.scale5x.Location = New System.Drawing.Point(56, 20)
        Me.scale5x.Name = "scale5x"
        Me.scale5x.Size = New System.Drawing.Size(37, 19)
        Me.scale5x.TabIndex = 1
        Me.scale5x.TabStop = True
        Me.scale5x.Text = "5x"
        Me.scale5x.UseVisualStyleBackColor = True
        '
        'scale3x
        '
        Me.scale3x.AutoSize = True
        Me.scale3x.Location = New System.Drawing.Point(6, 20)
        Me.scale3x.Name = "scale3x"
        Me.scale3x.Size = New System.Drawing.Size(37, 19)
        Me.scale3x.TabIndex = 0
        Me.scale3x.TabStop = True
        Me.scale3x.Text = "3x"
        Me.scale3x.UseVisualStyleBackColor = True
        '
        'speedUp
        '
        Me.speedUp.AutoSize = True
        Me.speedUp.Location = New System.Drawing.Point(81, 169)
        Me.speedUp.Name = "speedUp"
        Me.speedUp.Size = New System.Drawing.Size(81, 19)
        Me.speedUp.TabIndex = 10
        Me.speedUp.Text = "Speed Up"
        Me.speedUp.UseVisualStyleBackColor = True
        '
        'Config
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(176, 196)
        Me.Controls.Add(Me.speedUp)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.hasWall)
        Me.Controls.Add(Me.rightBox)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.leftBox)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.downBox)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.upBox)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "Config"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Config"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents upBox As System.Windows.Forms.TextBox
    Friend WithEvents downBox As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents leftBox As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents rightBox As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents hasWall As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents scale10x As System.Windows.Forms.RadioButton
    Friend WithEvents scale5x As System.Windows.Forms.RadioButton
    Friend WithEvents scale3x As System.Windows.Forms.RadioButton
    Friend WithEvents speedUp As System.Windows.Forms.CheckBox
End Class
